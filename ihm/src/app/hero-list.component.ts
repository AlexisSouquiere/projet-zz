import {OnInit, Component} from "@angular/core";
import {Hero} from "./hero";
import {HeroService} from "./hero.service";

/**
 * Created by alexis on 15/11/2016.
 */

@Component({
  selector: 'hero-list',
  templateUrl: './hero-list.component.html'
})
export class HeroListComponent implements OnInit {
  heroes: Hero[];

  mode = 'Observable';

  constructor (private heroService: HeroService) {}

  ngOnInit() { this.getHeroes(); }

  getHeroes() {
    this.heroService.getHeroes()
      .subscribe(
        heroes => this.heroes = heroes);
  }
}
