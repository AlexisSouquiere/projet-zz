package com.ssg.zz.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZzProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZzProjectApplication.class, args);
	}
}
