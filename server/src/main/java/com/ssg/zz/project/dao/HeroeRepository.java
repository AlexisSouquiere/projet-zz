package com.ssg.zz.project.dao;

import com.ssg.zz.project.model.Heroe;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by alexis on 15/11/2016.
 */
public interface HeroeRepository extends CrudRepository<Heroe, Long> {

}
