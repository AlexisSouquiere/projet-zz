package com.ssg.zz.project.rest;

import com.ssg.zz.project.dao.HeroeRepository;
import com.ssg.zz.project.model.Heroe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

/**
 * Created by alexis on 15/11/2016.
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@Controller
@RequestMapping("/heroes")
public class HeroeController {

    @Autowired
    private HeroeRepository heroeRepository;

    @RequestMapping(method= RequestMethod.GET)
    public @ResponseBody List<Heroe> list() {
        return (List<Heroe>) heroeRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> save(@PathVariable String name) {
        Heroe heroe = heroeRepository.save(new Heroe(name));

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(heroe.getId()).toUri();

        return ResponseEntity.created(location).build();
    }
}
